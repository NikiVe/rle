<?php

use RLE\Basic;
use Symfony\Component\Console\Application;

require_once __DIR__ . '/vendor/autoload.php';

$GLOBALS['DIR'] = __DIR__;

$app = new Application();
$app->add(new \RLE\Command\Archive());
$app->add(new \RLE\Command\Dearchive());
$app->run();