<?php

namespace RLE;

class Modern
{
    const B_MAX_SIZE = 127;
    const MAX_REPEAT = 4;

    public function __construct() {}

    public function archive(string $inputFile, string $outputFile) {
        $rH = fopen($inputFile, "rb");
        $wH = fopen($outputFile, "wb");

        $buffer = new Buffer(self::B_MAX_SIZE);

        $last = null;
        $counter = 1;

        do {
            $sym = fread($rH, '1');

            if ($sym === $last) {
                $counter++;
            } elseif($last !== null) {
                if ($counter <= self::MAX_REPEAT) {
                    while ($counter) {
                        for (; $counter && $buffer->cadAdd(); $counter--) {
                            $buffer->add($last);
                        }
                        if (!$buffer->cadAdd()) {
                            $bs = $buffer->getSize();
                            self::save($wH, $buffer->getAll(), -$bs);
                        }
                    }
                    $counter = 1;
                } else {
                    if ($buffer->getSize()) {
                        $bs = $buffer->getSize();
                        self::save($wH, $buffer->getAll(), -$bs);
                    }
                    self::save($wH, $last, $counter);
                    $counter = 1;
                }
            }
            $last = $sym;
        } while (!feof($rH));
        if ($bs = $buffer->getSize()) {
            self::save($wH, $buffer->getAll(), -$bs);
        }
        fclose($rH);
        fclose($wH);
    }

    protected static function save($wH, string $string, $counter) {
        $bc = self::packCounter($counter);

        fwrite($wH, $bc);
        fwrite($wH, $string);
    }

    public static function packCounter(int $counter) {
        if ($counter < 0) {
            $counter = abs($counter) | 128;
        }
        return pack('C', $counter);
    }

    public static function unpackCounter(string $counter, int &$isPositive) {
        $counter = unpack('C', $counter);
        $counter = $counter[1];
        if ($counter > 127) {
            $counter = $counter & 127;
            $isPositive = false;
        } else {
            $isPositive = true;
        }
        return $counter;
    }

    public function dearchive(string $inputFile, string $outputFile) {
        $rH = fopen($inputFile, "rb");
        $wH = fopen($outputFile, "wb");
        $isPositive = false;

        do {
            $bc = fread($rH, '1');
            if ($bc !== false && $bc !== "") {
                $counter = self::unpackCounter($bc, $isPositive);

                if ($isPositive) {
                    $sym = fread($rH, '1');
                    for ($i = 0; $i < $counter; $i++) {
                        fwrite($wH, $sym);
                    }
                } else {
                    for ($i = 0; $i < $counter; $i++) {
                        $sym = fread($rH, '1');
                        fwrite($wH, $sym);
                    }
                }
            }

        } while (!feof($rH));

        fclose($rH);
        fclose($wH);
    }
}