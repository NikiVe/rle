<?php

namespace RLE;

class Basic
{
    public function __construct() {

    }

    public function archive(string $inputFile, string $outputFile) {
        $rH = fopen($inputFile, "rb");
        $wH = fopen($outputFile, "wb");

        $last = null;
        $counter = 1;

        do {
            $sym = fread($rH, '1');

            if ($sym === $last) {
                $counter++;
            } elseif($last !== null) {
                $bc = pack('C', $counter);

                fwrite($wH, $bc);
                fwrite($wH, $last);

                $counter = 1;
            }
            $last = $sym;
        } while (!feof($rH));
        fclose($rH);
        fclose($wH);
    }

    public function dearchive(string $inputFile, string $outputFile) {
        $rH = fopen($inputFile, "rb");
        $wH = fopen($outputFile, "wb");

        do {
            $bc = fread($rH, '1');
            $sym = fread($rH, '1');
            if ($sym !== false && $bc !== false && $bc !== "") {
                $counter = unpack('C', $bc);
                for ($i = 0; $i < $counter[1]; $i++) {
                    fwrite($wH, $sym);
                }
            }

        } while (!feof($rH));

        fclose($rH);
        fclose($wH);
    }
}