<?php


namespace RLE;


class Buffer
{
    protected string $buffer = '';
    protected int $size = 0;
    protected int $maxSize = 0;

    public function __construct(int $maxSize = 127) {
        $this->maxSize = $maxSize;
    }

    public function add(string $e) {
        if ($this->size === 0) {
            $this->buffer = '';
        }
        if (!$this->cadAdd()) {
            throw new \Exception('Overflow!');
        }
        $this->buffer .= $e;
        $this->size += 1;
    }

    public function getAll() : string {
        $this->size = 0;
        return $this->buffer;
    }

    public function getSize() : int {
        return $this->size;
    }

    public function getBalance() {
        return $this->maxSize - $this->size;
    }

    public function cadAdd(int $size = 1) {
        return $this->getBalance() - $size > 0;
    }
}