<?php


namespace RLE\Command;


class Helper
{
    public static function getPath(string $path) : string {
        $f = substr($path, 0, 1);
        $f = str_replace('\\', '/', $f);
        if ($f !== '/') {
            $path = $GLOBALS['DIR'] . '/' . $path;
        }
        return $path;
    }
}