<?php

namespace RLE\Command;

use RLE\Basic;
use RLE\Modern;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Archive extends Command
{
    protected function configure() {
        $this->setName('archive')
        ->setDescription('Архивация файла')
        ->setDefinition(new InputDefinition([
            new InputArgument('input', InputArgument::REQUIRED),
            new InputArgument('output', InputArgument::REQUIRED),
            new InputOption('basic', 'b', InputOption::VALUE_OPTIONAL),
        ]));
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $inputFileName = Helper::getPath($input->getArgument('input'));
        $outputFileName = Helper::getPath($input->getArgument('output'));

        if ($input->getOption('basic') === 'basic') {
            $app = new Basic();
        } else {
            $app = new Modern();
        }

        $sT = microtime(true);
        $app->archive($inputFileName, $outputFileName);
        echo 'Время работы: ' . round(microtime(true) - $sT, 4);
        return 0;
    }
}